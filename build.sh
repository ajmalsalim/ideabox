#!/bin/sh


# The primary duty of this script is accepting the Android SDK license and start the build of our project: in the example, we invoke the assembleDebug task, but you can use whatever you need.

mkdir "${ANDROID_HOME}/licenses" || true
printf "8933bad161af4178b1185d1a37fbf41ea5269c55\nd56f5187479451eabf01fb78af6dfcb131a6481e" > "${ANDROID_HOME}/licenses/android-sdk-license"

./gradlew assembleDebug