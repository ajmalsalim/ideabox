package test.com.ideabox.ui.idea;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;
import test.com.ideabox.R;
import test.com.ideabox.data.db.entities.Idea;

/*
 * Adapter for RecyclerView
 * */

public class IdeaAdapter extends ListAdapter<Idea, IdeaAdapter.IdeaViewHolder> {

    private OnItemClickListener listener;

    public IdeaAdapter() {
        super(DIFF_CALLBACK);
    }

    //the comparison logic. DiffUtil compares two lists.
    private static final DiffUtil.ItemCallback<Idea> DIFF_CALLBACK = new DiffUtil.ItemCallback<Idea>() {
        @Override
        public boolean areItemsTheSame(@NonNull Idea oldItem, @NonNull Idea newItem) {
            return oldItem.getId() == newItem.getId();
        }


        /*
         * LiveData returns new list every time so need to do check all fields manually
         * */
        @Override
        public boolean areContentsTheSame(@NonNull Idea oldItem, @NonNull Idea newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) &&
                    oldItem.getProblem().equals(newItem.getProblem()) &&
                    oldItem.getCustomerSegments().equals(newItem.getCustomerSegments()) &&
                    oldItem.getUniqueValueProposition().equals(newItem.getUniqueValueProposition()) &&
                    oldItem.getSolution().equals(newItem.getSolution()) &&
                    oldItem.getChannels().equals(newItem.getChannels()) &&
                    oldItem.getRevenueStreams().equals(newItem.getRevenueStreams()) &&
                    oldItem.getCostStructures().equals(newItem.getCostStructures()) &&
                    oldItem.getKeyMetrics().equals(newItem.getKeyMetrics()) &&
                    oldItem.getUnfairAdvantage().equals(newItem.getUnfairAdvantage());
        }
    };

    @NonNull
    @Override
    public IdeaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.idea_item, parent, false);

        return new IdeaViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull IdeaViewHolder holder, int position) {

        Idea currentIdea = getItem(position);
        holder.textViewTitle.setText(currentIdea.getTitle());
        holder.textViewDescription.setText(getIdeaPreview(currentIdea));

    }


    /*
     * ViewHolder for RecyclerView
     * */

    class IdeaViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewTitle;
        private TextView textViewDescription;

        public IdeaViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.text_view_idea_item_title);
            textViewDescription = itemView.findViewById(R.id.text_view_idea_item_desc);

            itemView.setOnClickListener(view -> {
                int position = getAdapterPosition();
                if (listener != null && position != RecyclerView.NO_POSITION) {
                    listener.onItemClick(getItem(position));
                }
            });
        }
    }

    public interface OnItemClickListener {
        void onItemClick(Idea idea);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    /*
     * Prepares a preview of the idea
     * */

    private String getIdeaPreview(Idea idea) {

        //TODO : improve this by providing insightful preview

        return idea.getProblem();
    }

}
