package test.com.ideabox.ui;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import test.com.ideabox.data.IdeaDataSource;
import test.com.ideabox.data.IdeaRepository;
import test.com.ideabox.data.db.entities.Idea;

/*
 * ViewModel for Idea
 * */

public class IdeaViewModel extends AndroidViewModel implements IdeaDataSource {

    private IdeaRepository ideaRepository;
    private LiveData<List<Idea>> allIdeas;

    public IdeaViewModel(@NonNull Application application) {
        super(application);

        ideaRepository = new IdeaRepository(application);
        allIdeas = ideaRepository.getAllIdeas();
    }

    @Override
    public void insertIdea(Idea idea) {
        ideaRepository.insertIdea(idea);
    }

    @Override
    public void updateIdea(Idea idea) {
        ideaRepository.insertIdea(idea);
    }

    @Override
    public void deleteIdea(Idea idea) {
        ideaRepository.deleteIdea(idea);
    }

    @Override
    public LiveData<List<Idea>> getAllIdeas() {
        return allIdeas;
    }
}
