package test.com.ideabox.ui.idea;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import test.com.ideabox.R;
import test.com.ideabox.data.db.entities.Idea;
import test.com.ideabox.ui.IdeaViewModel;

/*
 * View for HomeScreen. Shows all the ideas.
 * */

public class IdeaActivity extends AppCompatActivity {

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.lottieAnimation)
    LottieAnimationView emptyBox;

    private int INSERT_REQ_CODE = 1;
    private int UPDATE_REQ_CODE = 2;

    private IdeaViewModel ideaViewModel;

    public static final String INTENT_IDEA = "idea";
    public static final String INTENT_ID = "id";

    private Idea sentIdea;

    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idea);

        ButterKnife.bind(this);

        setupViews();
    }

    private void setupViews() {

        setupRecyclerView();

        setupFloatingActionButton();

    }

    private void setupRecyclerView() {

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        IdeaAdapter ideaAdapter = new IdeaAdapter();
        recyclerView.setAdapter(ideaAdapter);

        ideaViewModel = ViewModelProviders.of(this).get(IdeaViewModel.class);

        //LiveData method - updated every time when the LiveData changes
        ideaViewModel.getAllIdeas().observe(this, new Observer<List<Idea>>() {
            @Override
            public void onChanged(List<Idea> ideas) {
                ideaAdapter.submitList(ideas);

                if (ideas.isEmpty()) {
                    emptyBox.setVisibility(View.VISIBLE);
                } else {
                    emptyBox.setVisibility(View.GONE);
                }
            }
        });

        //click listener method
        ideaAdapter.setOnItemClickListener(idea -> {


            Intent intent = new Intent(IdeaActivity.this, IdeaDetailsActivity.class);
            intent.putExtra("id", idea.getId());

            intent.putExtra(INTENT_IDEA, idea);

            Log.d(TAG, "Sent : " + idea.toString());

            sentIdea = idea;

            startActivityForResult(intent, UPDATE_REQ_CODE);
        });

    }

    private void setupFloatingActionButton() {

        fab.setOnClickListener(view -> {

            Intent intent = new Intent(IdeaActivity.this, IdeaDetailsActivity.class);
            startActivityForResult(intent, INSERT_REQ_CODE);


        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == INSERT_REQ_CODE && resultCode == RESULT_OK) {


            //save idea
            if (data != null) {
                Idea idea = data.getParcelableExtra(INTENT_IDEA);

                ideaViewModel.insertIdea(idea);

                Toast.makeText(this, getString(R.string.idea_saved_msg), Toast.LENGTH_SHORT).show();
            }


        } else if (requestCode == UPDATE_REQ_CODE && resultCode == RESULT_OK) {

            //update idea

            if (data != null) {


                Idea idea = data.getParcelableExtra(INTENT_IDEA);


                int id = data.getIntExtra(INTENT_ID, -1);

                if (id == -1) {
                    Toast.makeText(this, "Can't update", Toast.LENGTH_SHORT).show();
                    return;
                }

                idea.setId(id);

                //update if there is any change
                if (!areSameIdeas(sentIdea, idea)) {
                    ideaViewModel.updateIdea(idea);

                    Toast.makeText(this, getString(R.string.idea_updated_msg), Toast.LENGTH_SHORT).show();
                }


            }

        }

    }

    /*
     * Compare contents
     * */

    private boolean areSameIdeas(Idea oldItem, Idea newItem) {
        return oldItem.getTitle().equals(newItem.getTitle()) &&
                oldItem.getProblem().equals(newItem.getProblem()) &&
                oldItem.getCustomerSegments().equals(newItem.getCustomerSegments()) &&
                oldItem.getUniqueValueProposition().equals(newItem.getUniqueValueProposition()) &&
                oldItem.getSolution().equals(newItem.getSolution()) &&
                oldItem.getChannels().equals(newItem.getChannels()) &&
                oldItem.getRevenueStreams().equals(newItem.getRevenueStreams()) &&
                oldItem.getCostStructures().equals(newItem.getCostStructures()) &&
                oldItem.getKeyMetrics().equals(newItem.getKeyMetrics()) &&
                oldItem.getUnfairAdvantage().equals(newItem.getUnfairAdvantage()) &&
                oldItem.getId() == newItem.getId();
    }
}
