package test.com.ideabox.ui.idea;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import butterknife.BindView;
import butterknife.ButterKnife;
import test.com.ideabox.R;
import test.com.ideabox.data.db.entities.Idea;
import test.com.ideabox.ui.IdeaViewModel;
import test.com.ideabox.util.Validation;

public class IdeaDetailsActivity extends AppCompatActivity {


    @BindView(R.id.edit_text_idea_details_title)
    EditText editTextTitle;

    @BindView(R.id.edit_text_idea_details_problem)
    EditText editTextProblem;

    @BindView(R.id.edit_text_idea_details_customer_segments)
    EditText editTextCustomerSegments;

    @BindView(R.id.edit_text_idea_details_unique_vp)
    EditText editTextUniqueValueProposition;

    @BindView(R.id.edit_text_idea_details_solution)
    EditText editTextSolution;

    @BindView(R.id.edit_text_idea_details_channels)
    EditText editTextChannels;

    @BindView(R.id.edit_text_idea_details_revenue_streams)
    EditText editTextRevenueStreams;

    @BindView(R.id.edit_text_idea_details_cost_structures)
    EditText editTextCostStructures;

    @BindView(R.id.edit_text_idea_details_key_metrics)
    EditText editTextKeyMetrics;

    @BindView(R.id.edit_text_idea_details_unfair_adv)
    EditText editTextUnfairAdvantage;


    private IdeaViewModel ideaViewModel;
    private Idea idea;
    private Intent intent;

    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idea_details);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ButterKnife.bind(this);

        ideaViewModel = ViewModelProviders.of(this).get(IdeaViewModel.class);

        intent = getIntent();

        readIntentData();

    }


    private void readIntentData() {

        if (intent.getParcelableExtra(IdeaActivity.INTENT_IDEA) != null) {

            //get idea from intent
            idea = intent.getParcelableExtra(IdeaActivity.INTENT_IDEA);

            //TODO : Debug this
            //for some strange errors, the id is getting resetting to 0.
            idea.setId(intent.getIntExtra("id", idea.getId()));

            Log.d(TAG, "Received : " + idea.toString());

            setTitle(R.string.toolbar_title_details);

            showIntentData();

        } else {

            //update title and fab
            setTitle(R.string.toolbar_title_new);
        }
    }

    private void showIntentData() {

        //display idea
        editTextTitle.setText(idea.getTitle());
        editTextProblem.setText(idea.getProblem());
        editTextCustomerSegments.setText(idea.getCustomerSegments());
        editTextUniqueValueProposition.setText(idea.getUniqueValueProposition());
        editTextSolution.setText(idea.getSolution());
        editTextChannels.setText(idea.getChannels());
        editTextRevenueStreams.setText(idea.getRevenueStreams());
        editTextCostStructures.setText(idea.getCostStructures());
        editTextKeyMetrics.setText(idea.getKeyMetrics());
        editTextUnfairAdvantage.setText(idea.getUnfairAdvantage());


    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        //hiding delete option for new idea
        if (!intent.hasExtra(IdeaActivity.INTENT_IDEA)) {
            menu.findItem(R.id.delete).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.idea_details_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:

                onBackPressed();

                break;
            case R.id.delete:

                deleteNote();

                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void deleteNote() {

        //show confirmation dialog

        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.delete_dialog_title))
                .setMessage(getString(R.string.delete_dialog_msg))
                .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {

                    Log.d(TAG, "Deleting : " + idea.toString());

                    //delete note
                    ideaViewModel.deleteIdea(idea);

                    finish();
                })
                .setNegativeButton(android.R.string.no, (dialogInterface, i) -> {
                    //do nothing
                })
                .show();
    }


    @Override
    public void onBackPressed() {

        prepareIntent();

        super.onBackPressed();

    }

    /*
     * Prepares the intent data to be saved
     * */

    private void prepareIntent() {

        if (isEligibleForSaving()) {

            //set title to unnamed if the user does not provide any name
            String title = editTextTitle.getText().toString();
            if (!Validation.isValidString(title)) {
                title = getResources().getString(R.string.unnamed);
            }

            String problem = editTextProblem.getText().toString();
            String customerSegments = editTextCustomerSegments.getText().toString();
            String uniqueVp = editTextUniqueValueProposition.getText().toString();
            String solution = editTextSolution.getText().toString();
            String channels = editTextChannels.getText().toString();
            String revenueStreams = editTextRevenueStreams.getText().toString();
            String costStructures = editTextCostStructures.getText().toString();
            String keyMetrics = editTextKeyMetrics.getText().toString();
            String unfairAdvantage = editTextUnfairAdvantage.getText().toString();


            Idea newIdea = new Idea(title, problem, customerSegments, uniqueVp, solution, channels, revenueStreams, costStructures, keyMetrics, unfairAdvantage);

            //prepare the intent
            Intent data = new Intent();
            data.putExtra(IdeaActivity.INTENT_IDEA, newIdea);

            int id = getIntent().getIntExtra(IdeaActivity.INTENT_ID, -1);
            if (id != -1) {
                data.putExtra(IdeaActivity.INTENT_ID, id);
            }

            setResult(RESULT_OK, data);


        }
    }

    /*
     * Checks if any of the fields are valid
     * */

    private boolean isEligibleForSaving() {

        return Validation.isValidString(editTextTitle.getText().toString()) ||
                Validation.isValidString(editTextProblem.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString()) ||
                Validation.isValidString(editTextCustomerSegments.getText().toString());

    }

}