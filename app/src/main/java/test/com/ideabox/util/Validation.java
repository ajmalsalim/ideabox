package test.com.ideabox.util;

public class Validation {

    //checking if string is valid
    public static boolean isValidString(String string) {
        return string != null && !string.trim().isEmpty() && !string.equals("null");
    }

}
