package test.com.ideabox.data.db.entities;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * Immutable model class for an Idea
 */

@Entity(tableName = "ideas")
public class Idea implements Parcelable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    private String title;

    private String problem;

    @ColumnInfo(name = "customer_segments")
    private String customerSegments;

    @ColumnInfo(name = "unique_value_proposition")
    private String uniqueValueProposition;

    private String solution;

    private String channels;

    @ColumnInfo(name = "revenue_streams")
    private String revenueStreams;

    @ColumnInfo(name = "cost_structures")
    private String costStructures;

    @ColumnInfo(name = "key_metrics")
    private String keyMetrics;

    @ColumnInfo(name = "unfair_advantage")
    private String unfairAdvantage;

    public Idea() {
    }

    public Idea(String title, String problem, String customerSegments, String uniqueValueProposition, String solution, String channels, String revenueStreams, String costStructures, String keyMetrics, String unfairAdvantage) {
        this.title = title;
        this.problem = problem;
        this.customerSegments = customerSegments;
        this.uniqueValueProposition = uniqueValueProposition;
        this.solution = solution;
        this.channels = channels;
        this.revenueStreams = revenueStreams;
        this.costStructures = costStructures;
        this.keyMetrics = keyMetrics;
        this.unfairAdvantage = unfairAdvantage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getProblem() {
        return problem;
    }

    public void setProblem(String problem) {
        this.problem = problem;
    }

    public String getCustomerSegments() {
        return customerSegments;
    }

    public void setCustomerSegments(String customerSegments) {
        this.customerSegments = customerSegments;
    }

    public String getUniqueValueProposition() {
        return uniqueValueProposition;
    }

    public void setUniqueValueProposition(String uniqueValueProposition) {
        this.uniqueValueProposition = uniqueValueProposition;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    public String getChannels() {
        return channels;
    }

    public void setChannels(String channels) {
        this.channels = channels;
    }

    public String getRevenueStreams() {
        return revenueStreams;
    }

    public void setRevenueStreams(String revenueStreams) {
        this.revenueStreams = revenueStreams;
    }

    public String getCostStructures() {
        return costStructures;
    }

    public void setCostStructures(String costStructures) {
        this.costStructures = costStructures;
    }

    public String getKeyMetrics() {
        return keyMetrics;
    }

    public void setKeyMetrics(String keyMetrics) {
        this.keyMetrics = keyMetrics;
    }

    public String getUnfairAdvantage() {
        return unfairAdvantage;
    }

    public void setUnfairAdvantage(String unfairAdvantage) {
        this.unfairAdvantage = unfairAdvantage;
    }

    @Override
    public String toString() {
        return "Idea{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", problem='" + problem + '\'' +
                ", customerSegments='" + customerSegments + '\'' +
                ", uniqueValueProposition='" + uniqueValueProposition + '\'' +
                ", solution='" + solution + '\'' +
                ", channels='" + channels + '\'' +
                ", revenueStreams='" + revenueStreams + '\'' +
                ", costStructures='" + costStructures + '\'' +
                ", keyMetrics='" + keyMetrics + '\'' +
                ", unfairAdvantage='" + unfairAdvantage + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Idea)) return false;
        Idea idea = (Idea) o;
        return id == idea.id &&
                Objects.equals(title, idea.title) &&
                Objects.equals(problem, idea.problem) &&
                Objects.equals(customerSegments, idea.customerSegments) &&
                Objects.equals(uniqueValueProposition, idea.uniqueValueProposition) &&
                Objects.equals(solution, idea.solution) &&
                Objects.equals(channels, idea.channels) &&
                Objects.equals(revenueStreams, idea.revenueStreams) &&
                Objects.equals(costStructures, idea.costStructures) &&
                Objects.equals(keyMetrics, idea.keyMetrics) &&
                Objects.equals(unfairAdvantage, idea.unfairAdvantage);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, title, problem, customerSegments, uniqueValueProposition, solution, channels, revenueStreams, costStructures, keyMetrics, unfairAdvantage);
    }

    @Override
    public int describeContents() {
        return 0;
    }


    protected Idea(Parcel in) {
        title = in.readString();
        problem = in.readString();
        customerSegments = in.readString();
        uniqueValueProposition = in.readString();
        solution = in.readString();
        channels = in.readString();
        revenueStreams = in.readString();
        costStructures = in.readString();
        keyMetrics = in.readString();
        unfairAdvantage = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(problem);
        dest.writeString(customerSegments);
        dest.writeString(uniqueValueProposition);
        dest.writeString(solution);
        dest.writeString(channels);
        dest.writeString(revenueStreams);
        dest.writeString(costStructures);
        dest.writeString(keyMetrics);
        dest.writeString(unfairAdvantage);
    }

    public static final Creator<Idea> CREATOR = new Creator<Idea>() {
        @Override
        public Idea createFromParcel(Parcel in) {
            return new Idea(in);
        }

        @Override
        public Idea[] newArray(int size) {
            return new Idea[size];
        }
    };
}
