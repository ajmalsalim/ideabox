package test.com.ideabox.data.db;


import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import test.com.ideabox.data.db.entities.Idea;

/**
 * Data Access Object for the ideas table.
 */

@Dao
public interface IdeaDao {


    /**
     * Insert an idea in the database. If the idea already exists, replace it.
     *
     * @param idea the idea to be inserted.
     */

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertIdea(Idea idea);

    /**
     * Update an idea in the database.
     *
     * @param idea the idea to be updated.
     */

    @Update
    void updateIdea(Idea idea);


    /**
     * Delete an idea in the database.
     *
     * @param idea the idea to be updated.
     */

    @Delete
    void deleteIdea(Idea idea);

    /**
     * Get all the ideas from the table. Here we use LiveData so that we can observe for any data changes.
     *
     * @return all the ideas from the table
     */
    @Query("SELECT * FROM ideas ORDER BY id DESC")
    LiveData<List<Idea>> getAllIdeas();


    /**
     * Get a specific idea from the table.
     *
     * @param id the id of the specific idea
     * @return idea with the specific id
     */
    @Query("SELECT * FROM ideas WHERE id = :id")
    LiveData<Idea> getIdeaById(int id);


}
