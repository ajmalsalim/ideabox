package test.com.ideabox.data;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;
import test.com.ideabox.data.db.entities.Idea;

public interface IdeaDataSource {

    /**
     * Insert an idea in the database. If the idea already exists, replace it.
     *
     * @param idea the idea to be inserted.
     */

    void insertIdea(Idea idea);

    /**
     * Update an idea in the database.
     *
     * @param idea the idea to be updated.
     */

    void updateIdea(Idea idea);


    /**
     * Delete an idea in the database.
     *
     * @param idea the idea to be updated.
     */

    void deleteIdea(Idea idea);

    /**
     * Get all the ideas from the table. Here we use LiveData so that we can observe for any data changes.
     *
     * @return all the ideas from the table
     */

    LiveData<List<Idea>> getAllIdeas();

}
