package test.com.ideabox.data.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import test.com.ideabox.data.db.entities.Idea;

/**
 * The Room database that contains the ideas table
 */
@Database(entities = {Idea.class}, version = 1)
public abstract class IdeasDatabase extends RoomDatabase {

    private static volatile IdeasDatabase INSTANCE;
    private static final String DB_NAME = "ideas.db";

    public abstract IdeaDao ideaDao();

    public static IdeasDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (IdeasDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            IdeasDatabase.class, DB_NAME)
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

}
