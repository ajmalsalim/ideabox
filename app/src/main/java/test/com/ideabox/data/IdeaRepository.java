package test.com.ideabox.data;

import android.app.Application;

import java.util.List;

import androidx.lifecycle.LiveData;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import test.com.ideabox.data.db.IdeaDao;
import test.com.ideabox.data.db.IdeasDatabase;
import test.com.ideabox.data.db.entities.Idea;

public class IdeaRepository implements IdeaDataSource {

    private final IdeaDao ideaDao;
    private final LiveData<List<Idea>> ideas;

    public IdeaRepository(Application application) {
        IdeasDatabase database = IdeasDatabase.getInstance(application);
        ideaDao = database.ideaDao();
        ideas = ideaDao.getAllIdeas();
    }

    @Override
    public void insertIdea(final Idea idea) {

        //using RxJava to do the operation in background
        final Disposable subscribe = Observable.fromCallable(() -> {

            ideaDao.insertIdea(idea);

            return false;

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Boolean result) -> {
                    //Use result for something
                });

    }

    @Override
    public void updateIdea(Idea idea) {

        final Disposable subscribe = Observable.fromCallable(() -> {

            ideaDao.updateIdea(idea);

            return false;

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Boolean result) -> {
                    //Use result for something
                });

    }

    @Override
    public void deleteIdea(Idea idea) {

        final Disposable subscribe = Observable.fromCallable(() -> {

            ideaDao.deleteIdea(idea);

            return false;

        }).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe((Boolean result) -> {
                    //Use result for something
                });

    }

    @Override
    public LiveData<List<Idea>> getAllIdeas() {
        return ideas;
    }
}
