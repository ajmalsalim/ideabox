package test.com.ideabox.data.mock;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import androidx.annotation.VisibleForTesting;
import androidx.lifecycle.LiveData;
import test.com.ideabox.data.IdeaDataSource;
import test.com.ideabox.data.db.entities.Idea;

/*
 * Mocks Idea for testing
 * */

public class MockIdeaSource implements IdeaDataSource {

    private static MockIdeaSource INSTANCE;

    private static final Map<Integer, Idea> IDEAS_SERVICE_DATA = new LinkedHashMap<>();

    private static String FAKE_IDEA_TITLE_1 = "FAKE TITLE 1";
    private static String FAKE_IDEA_PROBLEM_1 = "FAKE PROBLEM 1";
    private static String FAKE_IDEA_CUST_SEG_1 = "FAKE CUSTOMER SEGMENT 1";
    private static String FAKE_IDEA_UNIQUE_VP_1 = "FAKE UNIQUE VALUE PROPOSITION 1";
    private static String FAKE_IDEA_SOLUTIONS_1 = "FAKE SOLUTIONS 1";
    private static String FAKE_IDEA_CHANNELS_1 = "FAKE CHANNELS 1";
    private static String FAKE_IDEA_REV_STREAMS_1 = "FAKE REVENUE STREAMS 1";
    private static String FAKE_IDEA_COST_STRUCT_1 = "FAKE COST STRUCTURE 1";
    private static String FAKE_IDEA_KEY_METRICS_1 = "FAKE KEY METRICS 1";
    private static String FAKE_IDEA_UNFAIR_ADV_1 = "FAKE UNFAIR ADVANTAGE 1";

    public static String FAKE_IDEA_UPDATED_TITLE = "FAKE IDEA UPDATED TITLE";

    private MockIdeaSource() {
    }

    public static MockIdeaSource getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new MockIdeaSource();
        }
        return INSTANCE;
    }

    @Override
    public void insertIdea(Idea idea) {
        IDEAS_SERVICE_DATA.put(idea.getId(), idea);
    }

    @Override
    public void updateIdea(Idea idea) {
        IDEAS_SERVICE_DATA.put(idea.getId(), idea);
    }

    @Override
    public void deleteIdea(Idea idea) {
        IDEAS_SERVICE_DATA.remove(idea);
    }

    @Override
    public LiveData<List<Idea>> getAllIdeas() {
        return null;
    }

    @VisibleForTesting
    public void addIdeas(Idea... ideas) {
        if (ideas != null) {
            for (Idea idea : ideas) {
                IDEAS_SERVICE_DATA.put(idea.getId(), idea);
            }
        }
    }

    public static Idea fetchFakeIdea() {

        Idea idea = new Idea(FAKE_IDEA_TITLE_1,
                FAKE_IDEA_PROBLEM_1,
                FAKE_IDEA_CUST_SEG_1,
                FAKE_IDEA_UNIQUE_VP_1,
                FAKE_IDEA_SOLUTIONS_1,
                FAKE_IDEA_CHANNELS_1,
                FAKE_IDEA_REV_STREAMS_1,
                FAKE_IDEA_COST_STRUCT_1,
                FAKE_IDEA_KEY_METRICS_1,
                FAKE_IDEA_UNFAIR_ADV_1
        );

        idea.setId(1);

        return idea;

    }

    public static List<Idea> getFakeIdeas(int size) {

        List<Idea> ideaList = new ArrayList<Idea>();

        for (int i = 1; i <= size; i++) {

            Idea idea = new Idea("Title " + i,
                    "Problem  " + i,
                    "Customer Seg " + i,
                    "Unique Val Prop " + i,
                    "Solution " + i,
                    "Channel " + i,
                    "Revenue Streams " + i,
                    "Cost Structures " + i,
                    "Key Metrics " + i,
                    "Unfair Adv " + i
            );

            idea.setId(i);

            ideaList.add(idea);
        }
        return ideaList;
    }
}
