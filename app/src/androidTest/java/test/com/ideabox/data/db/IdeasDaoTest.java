package test.com.ideabox.data.db;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import androidx.arch.core.executor.testing.InstantTaskExecutorRule;
import androidx.room.Room;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;
import test.com.ideabox.data.LiveDataTestUtil;
import test.com.ideabox.data.db.entities.Idea;
import test.com.ideabox.data.mock.MockIdeaSource;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertNotNull;
import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.assertNull;

@RunWith(AndroidJUnit4.class)
public class IdeasDaoTest {


    // Rule to make sure that Room executes all the database operations instantly
    @Rule
    public InstantTaskExecutorRule instantTaskExecutorRule = new InstantTaskExecutorRule();

    private IdeasDatabase database;
    private IdeaDao ideaDao;


    @Before
    public void initDb() throws Exception {
        database = Room.inMemoryDatabaseBuilder(
                InstrumentationRegistry.getInstrumentation().getTargetContext(),
                IdeasDatabase.class)
                // allowing main thread queries, just for testing
                .allowMainThreadQueries()
                .build();

        ideaDao = database.ideaDao();
    }


    @Test
    public void test_OnGetAllIdeas_ShouldReturnEmptyList_IfTable_IsEmpty() throws InterruptedException {

        //fetching all the ideas from db
        List<Idea> ideaList = LiveDataTestUtil.getValue(ideaDao.getAllIdeas());

        assertTrue(ideaList.isEmpty());
    }

    @Test
    public void test_OnInsertingAnIdea_CheckIf_InsertHappensCorrectly() throws InterruptedException {

        Idea idea = MockIdeaSource.fetchFakeIdea();

        ideaDao.insertIdea(idea);

        assertEquals(1, LiveDataTestUtil.getValue(ideaDao.getAllIdeas()).size());

        assertNotNull(LiveDataTestUtil.getValue(ideaDao.getIdeaById(1)));

    }

    @Test
    public void test_OnUpdatingAnIdea_CheckIf_UpdateHappensCorrectly() throws InterruptedException {

        Idea idea = MockIdeaSource.fetchFakeIdea();

        ideaDao.insertIdea(idea);

        //update the title
        idea.setTitle(MockIdeaSource.FAKE_IDEA_UPDATED_TITLE);
        ideaDao.updateIdea(idea);

        //see if the title is updated
        assertEquals(MockIdeaSource.FAKE_IDEA_UPDATED_TITLE,
                LiveDataTestUtil.getValue(ideaDao.getIdeaById(idea.getId())).getTitle());
    }

    @Test
    public void test_OnIdeaDeletion_CheckIf_DeleteHappensCorrectly() throws InterruptedException {

        List<Idea> ideaList = MockIdeaSource.getFakeIdeas(5);

        for (Idea idea : ideaList) {
            ideaDao.insertIdea(idea);
        }

        ideaDao.deleteIdea(ideaList.get(3));

        assertNull(LiveDataTestUtil.getValue(ideaDao.getIdeaById(ideaList.get(3).getId())));
    }

    @After
    public void closeDb() throws Exception {
        database.close();
    }

}
