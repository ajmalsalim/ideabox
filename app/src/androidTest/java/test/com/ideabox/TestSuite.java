package test.com.ideabox;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import test.com.ideabox.data.db.IdeasDaoTest;
import test.com.ideabox.ui.idea.IdeaActivityTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        IdeasDaoTest.class,
        IdeaActivityTest.class

})
public class TestSuite {

}