package test.com.ideabox.ui.idea;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import androidx.test.espresso.ViewInteraction;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import test.com.ideabox.R;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressBack;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.RootMatchers.withDecorView;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withParent;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.AllOf.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class IdeaActivityTest {

    @Rule
    public ActivityTestRule<IdeaActivity> mActivityTestRule = new ActivityTestRule<IdeaActivity>(IdeaActivity.class, true, false);


    @Before
    public void setUp() {
        mActivityTestRule.launchActivity(null);
    }


    @Test
    public void test_IfAddIdea_WorkingProperly() {


        //find fab
        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.fab),
                        withParent(allOf(withId(R.id.coordinator_layout),
                                withParent(withId(android.R.id.content)))),
                        isDisplayed()));

        //click on fab
        floatingActionButton.perform(click());

        //fill title edit text
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.edit_text_idea_details_title), isDisplayed()));
        appCompatEditText.perform(typeText("Title 1"), closeSoftKeyboard());

        //fill problem edittext
        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.edit_text_idea_details_problem), isDisplayed()));
        appCompatEditText2.perform(typeText("The Testing Problem"), closeSoftKeyboard());

        //come back to previous activity
        pressBack();

        //see if it is on the list
        onView(withText("The Testing Problem"))
                .inRoot(withDecorView(not(mActivityTestRule.getActivity().getWindow().getDecorView())))
                .check(matches(isDisplayed()));


    }


    @Test
    public void test_IfUpdateIdea_WorkingProperly() {


        //find recyclerview item
        onView(withId(R.id.recycler_view))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        //replace title edit text
        onView(
                allOf(withId(R.id.edit_text_idea_details_title), isDisplayed()))
                .perform(replaceText("Title 2"), closeSoftKeyboard());


        //fill problem edittext
        onView(
                allOf(withId(R.id.edit_text_idea_details_problem), isDisplayed()))
                .perform(replaceText("New Problem"), closeSoftKeyboard());

        //come back to previous activity
        pressBack();

        //see if it is on the list

        onView(withText("Title 2"))
                .inRoot(withDecorView(not(mActivityTestRule.getActivity().getWindow().getDecorView())))
                .check(matches(isDisplayed()));

        onView(withText("New Problem"))
                .inRoot(withDecorView(not(mActivityTestRule.getActivity().getWindow().getDecorView())))
                .check(matches(isDisplayed()));


    }


}
