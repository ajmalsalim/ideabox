package test.com.ideabox;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import test.com.ideabox.util.ValidationTest;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        ValidationTest.class,

})
public class TestSuite {

}