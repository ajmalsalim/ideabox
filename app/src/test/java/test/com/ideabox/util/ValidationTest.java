package test.com.ideabox.util;

import org.junit.Test;

import static org.junit.Assert.assertFalse;

public class ValidationTest {

    @Test
    public void testNullinIsValidStringShouldReturnFalse(){

        boolean result = Validation.isValidString(null);
        assertFalse("Null should return False",result);

        result = Validation.isValidString("null");
        assertFalse("Null should return False",result);
    }

    @Test
    public void testEmptyStringinIsValidStringShouldReturnFalse(){
        boolean result = Validation.isValidString("");
        assertFalse("Empty String should return False",result);

        result = Validation.isValidString(" ");
        assertFalse("Empty String should return False",result);
    }

}
